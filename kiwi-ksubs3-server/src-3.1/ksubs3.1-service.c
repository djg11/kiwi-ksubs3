// Kiwi Scientific Acceleration: Substrate Code
//
// (C) 2016 DJ Greaves, University of Cambridge, Computer Laboratory.
//
// Kiwi ksubs-server for ksubs3.1 protocol


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <ctype.h>
#include <stdint.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>

#include "ksubs-busaccess.h"  

int g_noc_debugverbosef = 0;
int g_fs_verbose = 0;



uint64_t target_read_result()
{
  uint32_t vlo = KSUBS3_READ32(KSUBS3_PIO_SPACE_RESULT_LO);
  uint32_t vhi  = KSUBS3_READ32(KSUBS3_PIO_SPACE_RESULT_HI);
 
  return ((uint64_t) vlo << 0) | ((uint64_t) vhi << 32);
}


int target_runstop(int cmd_code)
{
  int vx = KSUBS3_READ32(KSUBS3_PIO_SPACE_CTRL_REG);
  vx &= ~0x30;
  vx = vx | ((cmd_code & 3) << 4);
  KSUBS3_WRITE32(KSUBS3_PIO_SPACE_CTRL_REG, vx);
  if (g_noc_debugverbosef) printf("ctr code now is b 0x%x   cmd_code=%i\n", vx, cmd_code);
}


int performance_check = 0;

int main(int argc, char *argv[])
{

  while (argc > 1)
    {

      if (!strcmp(argv[1], "-noc-debug"))
	{
	  g_noc_debugverbosef = 1;
	  argc -= 1; argv += 1;
	  continue;
	}


      if (!strcmp(argv[1], "-pio-performance"))
	{
	  performance_check = atoi(argv[2]);
	  argc -= 2; argv += 2;
	  continue;
	}

      if (!strcmp(argv[1], "-fs-debug"))
	{
	  g_fs_verbose = atoi(argv[2]);
	  argc -= 2; argv += 2;
	  continue;
	}


      printf ("Ignored cmd line flag %s", argv[1]);
      break;
    }

  service_start();

  printf("Ksubs3.1-server: Loaded design serial no is 0x%x\n", KSUBS3_READ32(KSUBS3_PIO_SPACE_SERIAL_NO));

  if (performance_check)
    {
      printf("Start performance check %i\n", performance_check);
      run_performance_check(performance_check);
      printf("End performance check\n");
      return 0;
    }

  
  target_runstop(0); // Reset it
  usleep(10000);
  target_runstop(1); // Run
  int i, j;

  uint32_t rc = 0;
  for (i=0; ; i++)
    {
      rc = step_noc16_daemon();
      if (i== 100000) 
	{
	  rc = 1000; // Timeout
	}
      if (rc != 0 && rc != 0x80) break; // Kiwi abend codes
    }

  if (rc != 1000) printf("Target exits with return code rc=0x%i ... Done\n", rc);
  else printf("exit on loop timeout in server\n");


step_noc16_daemon();
step_noc16_daemon();

  uint64_t ans = target_read_result();
  printf("Result register answer is 0x%016llX\n", ans);
  return 0;
}

// eof
