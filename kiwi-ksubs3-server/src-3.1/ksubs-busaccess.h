#ifndef KSUBS_BUSACCESS_H
#define KSUBS_BUSACCESS_H

#include <stdint.h>

// Kiwi Scientific Acceleration: Substrate Code
//
// (C) 2016 DJ Greaves, University of Cambridge, Computer Laboratory.
//
// Kiwi ksubs-server for ksubs3.1 protocol


/* ksubs 3 memory map
 4  0x43c00010 mon1            
 5  0x43c00014 waypoint        
 6  0x43c00018 result_lo       
 7  0x43c0001c result_hi         
 8  0x43c00020 tx_Noc16_lo     // Data from FPGA is called TX
 9  0x43c00024 tx_Noc16_hi     
 10  0x43c00028 tx_Noc16_status  // Bit 8 indicates data is ready

 11  0x43c0002c <spare>        
 12  0x43c00020 rx_Noc16_lo     // Data to FPGA is called RX
 13  0x43c00024 rx_Noc16_hi  
 14  0x43c00028 rx_Noc16_status // Bit 8 indicates data can be sent
 15   0x43c0002c <spare>  
*/

#define KSUBS3_PIO_SPACE_SERIAL_NO 0
#define KSUBS3_PIO_SPACE_CTRL_REG  2

#define KSUBS3_PIO_SPACE_WAYPOINT  5
#define KSUBS3_PIO_SPACE_RESULT_LO 6
#define KSUBS3_PIO_SPACE_RESULT_HI 7


#define KSUBS3_PIO_SPACE_TX_NOC16_DATA_LO 8
#define KSUBS3_PIO_SPACE_TX_NOC16_DATA_HI 9
#define KSUBS3_PIO_SPACE_TX_NOC16_STATUS 10

#define KSUBS3_PIO_SPACE_RX_NOC16_DATA_LO 12
#define KSUBS3_PIO_SPACE_RX_NOC16_DATA_HI 13
#define KSUBS3_PIO_SPACE_RX_NOC16_STATUS 14


/* ksubs 3 memory map
 4  0x43c00010 mon1            
 5  0x43c00014 waypoint        
 6  0x43c00018 result_lo       
 7  0x43c0001c result_hi         
 8  0x43c00020 tx_Noc16_lo     // Data from FPGA is called TX
 9  0x43c00024 tx_Noc16_hi     
 10  0x43c00028 tx_Noc16_status  // Bit 8 indicates data is ready

 11  0x43c0002c <spare>        
 12  0x43c00020 rx_Noc16_lo     // Data to FPGA is called RX
 13  0x43c00024 rx_Noc16_hi  
 14  0x43c00028 rx_Noc16_status // Bit 8 indicates data can be sent
 15   0x43c0002c <spare>  
*/

#define KSUBS3_PIO_SPACE_SERIAL_NO 0

#define KSUBS3_PIO_SPACE_WAYPOINT  5
#define KSUBS3_PIO_SPACE_RESULT_LO 6
#define KSUBS3_PIO_SPACE_RESULT_HI 7


#define KSUBS3_PIO_SPACE_TX_NOC16_DATA_LO 8
#define KSUBS3_PIO_SPACE_TX_NOC16_DATA_HI 9
#define KSUBS3_PIO_SPACE_TX_NOC16_STATUS 10

#define KSUBS3_PIO_SPACE_RX_NOC16_DATA_LO 12
#define KSUBS3_PIO_SPACE_RX_NOC16_DATA_HI 13
#define KSUBS3_PIO_SPACE_RX_NOC16_STATUS 14

extern volatile int *ksubs3_map_base;
#define KSUBS3_READ32(regno) (ksubs3_map_base[(regno)])
#define KSUBS3_WRITE32(regno, DD) (ksubs3_map_base[(regno)] = (DD))


extern void run_performance_check(int performance_check);
extern uint32_t step_noc16_daemon();
extern uint64_t kiwifs_bev(uint8_t cmd, uint64_t perform_bevfs_op_a2);
extern int g_noc_debugverbosef;
extern int g_fs_verbose;
#endif
