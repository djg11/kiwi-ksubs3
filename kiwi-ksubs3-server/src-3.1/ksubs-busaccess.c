// Kiwi Scientific Acceleration: Substrate Code
//
// (C) 2016 DJ Greaves, University of Cambridge, Computer Laboratory.
//
// Kiwi ksubs-server for ksubs3.1 protocol


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <ctype.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/mman.h>

#define KSUBS_PROTOCOL PROTOCOL_3_1

#include "ksubs-busaccess.h"  

#define LL_TRC(X)

#define MAP_SIZE 4096UL
#define MAP_MASK (MAP_SIZE - 1)


void *pager_open(off_t target)
{
  static int fd = -1000;
  static unsigned int page = 0;
  static void *map_base = 0;
  unsigned long read_result;


  if (fd < 0)
    {
      fd = open("/dev/mem", O_RDWR | O_SYNC);
      if (fd < 0)
	{
	  printf("lowlevel_accessor failed %s:%i Am I running with root privelege?\n", __FILE__, __LINE__);
	  exit(1);
	}

      LL_TRC(printf("/dev/mem opened.\n"); fflush(stdout));
    }

  if (map_base)  // check page is still the correct one                                                                                                                                                       
    {
      unsigned int page_primed = target & ~MAP_MASK;
      if (page != page_primed)
	{
	  printf("lowlevel_accessor failed %s:%i new page requested : %x cf %x\n", __FILE__, __LINE__, page_primed, page);
	  exit(1);
	}
    }
  else
    {     /* Map one page */
      page = target & ~MAP_MASK;
      map_base = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, page);
      if (map_base == (void *) -1)
	{
	  printf("lowlevel_accessor failed %s:%i failed to map page\n", __FILE__, __LINE__);
	  exit(1);
	}
    }

  LL_TRC(printf("Memory mapped at address %p.\n", map_base); fflush(stdout));
  return map_base;
}

volatile int *ksubs3_map_base = 0;


unsigned int service_start()
{
  off_t target = 0x43c80000; //
  ksubs3_map_base = (int *) pager_open(target);

  printf("Device serial no %x\n", ((volatile int*)ksubs3_map_base)[0]);
  //void *virt_addr = map_base + (target & MAP_MASK);
  //((unsigned long *) virt_addr)[0] = (enablef) ? 1:0;
  return 0;
}

#if 1
void data_send_check32(int loops)
{
  int i, j;
  printf("32-bit mode\n");

  volatile unsigned int *map_base_u8 = (unsigned int *)ksubs3_map_base;
  int d = 0;
  for (i=0; i<loops; i++)
    {
      int c = map_base_u8[10];
	{
	  map_base_u8[16] = d;
	  map_base_u8[20] = d;
	  map_base_u8[24] = d;
	  map_base_u8[28] = ++d;
	  
	  asm volatile (" ");
	}
    }
}




void data_send_check64(int loops)
{
  int i, j;
  // 14 seconds to send 8x4 x 1e7 bytes = 320 MB  - slow.   22 MB/sec
  // A 32-bit interface clocked at 100 MHz has 400 MB/sec throughput raw.
  printf("64-bit mode\n");

  // Using 64 bit stores halved us to 7 seconds.
  volatile long long unsigned int *map_base_u8 = (long long unsigned int *)ksubs3_map_base;

    for (i=0; i<loops; i++)
    {
      long long unsigned int d = 0;
      map_base_u8[16] = d;
	  map_base_u8[20] = d;
	  map_base_u8[24] = d;
	  map_base_u8[28] = ++d;
	  
	  asm volatile (" ");    
    }
}

void count_test()
{
  int s = 2;
   for (int i=0;i<10000000;i++)
	{
	   int dx = i*31;
	   s+= dx;
	}
   printf("Computed Result %i   0x%x\n", s, s);

   for (int i=0;i<10000000;i++)
	{
	   int dx = i*31;
           KSUBS3_WRITE32(0x44/4, dx);
	}



}

void run_performance_check(int performance_check)
{
   count_test();
//  data_send_check32(performance_check);
}

#endif

// We send on the 'rx' side of the interface since the name refers to the direction on the FPGA side.
void  noc16_output_send(uint64_t din_lo, uint8_t cmd)
{
  while (1)
    {
      uint32_t cmd_status = KSUBS3_READ32(KSUBS3_PIO_SPACE_RX_NOC16_STATUS);
      if (cmd_status & (1<<8)) break;
      usleep(100);
    }
 
  KSUBS3_WRITE32(KSUBS3_PIO_SPACE_RX_NOC16_DATA_LO, din_lo >> 0);
  KSUBS3_WRITE32(KSUBS3_PIO_SPACE_RX_NOC16_DATA_HI, din_lo >> 32);
  KSUBS3_WRITE32(KSUBS3_PIO_SPACE_RX_NOC16_STATUS, cmd);
}

// We poll for input the 'tx' side of the interface since the name refers to the direction on the FPGA side.
int  noc16_input_poll(uint64_t *din_lo_p, uint8_t *cmd_p)
{
  uint32_t cmd_in = KSUBS3_READ32(KSUBS3_PIO_SPACE_TX_NOC16_STATUS);
  if (cmd_in & (1<<8))
    {
      *cmd_p = cmd_in & 0xFF;
      uint32_t lo = KSUBS3_READ32(KSUBS3_PIO_SPACE_TX_NOC16_DATA_LO);
      uint32_t hi = KSUBS3_READ32(KSUBS3_PIO_SPACE_TX_NOC16_DATA_HI);
      uint64_t din_lo = (((uint64_t)hi)<<32) | (((uint64_t)lo)<<0);
      *din_lo_p = din_lo;
      return 1;
    }
  else
    {
      *cmd_p = 0;
      *din_lo_p = 0;
      return 0;
    }
}


void display_main_vector()
{
 uint32_t waypoint = KSUBS3_READ32(KSUBS3_PIO_SPACE_WAYPOINT);
 uint32_t result_lo = KSUBS3_READ32(KSUBS3_PIO_SPACE_RESULT_LO);
 uint32_t result_hi = KSUBS3_READ32(KSUBS3_PIO_SPACE_RESULT_HI);
 printf ("main_vector: %02d  0x%08X 0x%08X", waypoint, result_hi, result_lo);
}


uint32_t step_noc16_daemon()
{
  uint64_t din_lo;
  uint8_t cmd;

  uint64_t out_data_lo = 0;
  uint8_t out_cmd = 0;

  int rc = noc16_input_poll(&din_lo, &cmd);
  //display_main_vector();
  if (rc)
    {
      if (g_noc_debugverbosef) printf(" RX %02X  %016llX\n", cmd, din_lo);
      out_cmd = 0;
      out_data_lo = 0;
      if (cmd && !(cmd & 0x10))
	{
	  uint64_t ans = kiwifs_bev(cmd, din_lo);
	  out_cmd = (cmd & 0xE0) | (0x10);
	  out_data_lo = ans;
	  if (g_noc_debugverbosef) printf(" TX %02X  %016llX\n", out_cmd, out_data_lo);
	}
    }
  noc16_output_send(out_data_lo, out_cmd);

  uint32_t vx = KSUBS3_READ32(KSUBS3_PIO_SPACE_CTRL_REG);
  //printf ("Code 0x%08x\n", vx);
  return vx >> 24; // Abend code
}

// eof

