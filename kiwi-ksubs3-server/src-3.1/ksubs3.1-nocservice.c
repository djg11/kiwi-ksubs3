// Kiwi Scientific Acceleration: Substrate Code
//
// (C) 2016 DJ Greaves, University of Cambridge, Computer Laboratory.
//
// Kiwi ksubs-server nocservice.c

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

#define KSUBS_PROTOCOL PROTOCOL_3_1

#include "ksubs-busaccess.h"

int verbose = 1;

#define Kiwi_fs_op_open     1
#define Kiwi_fs_console     2
#define Kiwi_fs_read_word   3
#define Kiwi_fs_write_word  4
#define Kiwi_fs_test_eof    5
#define Kiwi_fs_close       6
#define Kiwi_fs_length      7


#define Kiwi_stdio_fd       1
#define Kiwi_stderr_fd      2
#define MIN_USR_FD          4 // Descriptors below this one are system descriptors - stdio etc..                                     




// TODO replace with virtual circuit tag no.
#define MAX_OPEN_FILES  32

const int  max_open_files = 32;
FILE *file_handles[MAX_OPEN_FILES];
int file_eofs[MAX_OPEN_FILES];
int lengths[MAX_OPEN_FILES];
const int n = 100;

int free_fd = 0; // TODO Use a bit mask

char filename_buf[132];
int filename_ptr = 0;

uint64_t kiwifs_bev(uint8_t cmd, uint64_t perform_bevfs_op_a2) // not threadsafe/re-entrant TODO
{
  int perform_bevfs_op_cmd = cmd & 15;
  if (cmd == 0) return 0;
  switch (perform_bevfs_op_cmd)
    {



    case Kiwi_fs_length: // Operation 7 - get length of an open file.                  
      {   
	int cmd_fd = (perform_bevfs_op_a2 >> 56) & 0xF;
	uint64_t op_return = lengths[cmd_fd];
	if (g_fs_verbose > 0) printf("kiwifs_bev: cmd file length fd=%d ans=%d\n", cmd_fd, lengths[cmd_fd]);
	return op_return;
      }

    case Kiwi_fs_op_open:
      {
	if (g_fs_verbose > 0) printf("kiwifs_bev: cmd open file %d a2=%llX\n", perform_bevfs_op_cmd, perform_bevfs_op_a2);
	switch ((perform_bevfs_op_a2 >> 16) & 15)
	  {
	  case 1: // Subcode 1 Start 
	    {
	      free_fd = free_fd + 1; // TODO wastes an fd on Exists check.   
	      filename_ptr = 0;
	      return 0;
	    }
	case 2: // Next char of the file name
	  {
	    filename_buf[filename_ptr++] = (perform_bevfs_op_a2 & 0xFF);
	    return 0;
	  }
	case 3: // Do the open for read and make a note of the length and return the fd.
	  {
	    //$sformat(filename, "%0s", filename); // Discard leading nulls?         
	    filename_buf[filename_ptr++] = (char) 0;	    
	    if (g_fs_verbose > 0) printf("kiwifs_bev: Opening for read %s as fd=%d\n", filename_buf, free_fd);
	    file_eofs[free_fd] = 0;
	    file_handles[free_fd] = fopen(filename_buf, "rb");
	    if (g_fs_verbose > 0) printf("Kiwi fserver: open read verilog fd rc=%p Kiwi fd=%d\n", file_handles[free_fd], free_fd);
	    
	    //
	    // Position the file pointer at end of file
	    int result = fseek(file_handles[free_fd], 0, 2);
	    if (result == -1) {
	      printf("kiwifs_bev: oops, can't move file pointer\n");
	      perror("");
	      exit(1);
	    }
	    // 
	    // Discover where it is    
	    lengths[free_fd] = ftell(file_handles[free_fd]);
	    if (g_fs_verbose > 0) printf("kiwifs_bev: File has %0d bytes\n", lengths[free_fd]);
	    result = fseek(file_handles[free_fd], 0, 0); // Seek back to start 
	    uint64_t ans = (file_handles[free_fd] != 0) ? (uint64_t)free_fd << 56 : 1;
	    return ans;
	  }
	  
	  case 4: // Open For Write            
	  {
	    filename_buf[filename_ptr++] = (char) 0;	    
	    //	    $sformat(filename, "%0s", filename); // Discard leading nulls?       
	    printf("kiwifs_bev: Opening for write %s as fd=%d\n", filename_buf, free_fd);
	    file_handles[free_fd] = fopen(filename_buf, "wb");
	    printf("Kiwi fserver:open write verilog fd rc=%p Kiwi fd=%d\n", file_handles[free_fd], free_fd);
	    return (file_handles[free_fd] != 0) ? (uint64_t)free_fd << 56 : 1;
	  }
	  case 5: // Check Exists 
	  {
	    struct stat buf;
	    // $sformat(filename, "%0s", filename); // Discard leading nulls? 
	    filename_buf[filename_ptr++] = (char) 0;	    
	    int rc = stat(filename_buf, &buf);
	    if (g_fs_verbose > 0) printf("Kiwi fserver: check exists %s rc=%i\n", filename_buf, rc);
	    return (rc==0) ? 0: 1;
	  }
	}

      case Kiwi_fs_read_word:
	{
	  uint64_t pbuffer = 0;
	  // Read up to seven bytes and put count in top byte.
	  int cmd_fd = (perform_bevfs_op_a2 >> 56) & 0xF;
	  int max_rd_len = perform_bevfs_op_a2 & 0xFFFFffff;
	  if (max_rd_len > 7) max_rd_len = 7;
	  if (verbose >= 2) printf("Kiwi fserver: (verbose) read_word (max bytes %d) a2=%016llX cmd_fd=%d\n", max_rd_len, perform_bevfs_op_a2, cmd_fd);
	  int xc, breakf = 0;
	  for (xc=0; !breakf && xc<max_rd_len; xc = xc+1) {
	    if (file_eofs[cmd_fd] || feof(file_handles[cmd_fd])) {
	      file_eofs[cmd_fd] = 1;
	      breakf = 1;
	      xc = -1; // Part of doing a break
	      printf("Kiwi fserver: Read eof on %d\n", cmd_fd);
	    }
	    else {
	      uint8_t xd = (fgetc(file_handles[cmd_fd]) & 0xFF); // Dont use fgetc! please!
	      pbuffer = (xc == 0) ? xd : pbuffer  | ((uint64_t)xd << (8 * xc));
	      //printf("Kiwi fserver: pack bufferx xc=%d char=%x %s %x", xc, xd, pbuffer, pbuffer);
	      //printf("Kiwi fserver: Read xc=%d chars ' %x", xc, pbuffer);
	    }
	  }
	  return ((uint64_t)xc << 56) | pbuffer;
	}
	//printf("       (file read char %c %d/%d)", perform_bevfs_op_return, ptr, pbuffer); 
	
      case Kiwi_fs_write_word:
	{
	  int xc = 0;
	  int cmd_fd = (perform_bevfs_op_a2 >> 56) & 0x1F;
	  int wr_len = (perform_bevfs_op_a2 >> 61) & 0x07;
	  uint64_t pbuffer = perform_bevfs_op_a2 & ((1uLL << 56)-1uLL);
	  if (cmd_fd < MIN_USR_FD) {
	    //if (verbose >= 2)                                                                                               
	    printf("Kiwi fserver: (verbose) write_console (max bytes %d) %d a2=%016llX cmd_fd=%d\n", wr_len, perform_bevfs_op_cmd, perform_bevfs_op_a2, cmd_fd);
	  }
	  // Write up to seven bytes.
	  else {
	    if (verbose >= 2) printf("Kiwi fserver: (verbose) write_word (max bytes %d) %d a2=%016llX cmd_fd=%d\n", wr_len, perform_bevfs_op_cmd, perform_bevfs_op_a2, cmd_fd);
	    for (xc=0; xc<wr_len; xc = xc+1) {
	      int xd = pbuffer  >> (8 * xc);
	      xc = fputc(xd, file_handles[cmd_fd]);
	      //printf("Kiwi fserver: pack bufferx xc=%d char=%x %s %016llXx", xc, xd, pbuffer, pbuffer);
	      printf("Kiwi fserver: write xc=%d chars %016llXx\n", xc, pbuffer);
	    }
	  }
	  return (uint64_t)xc << 56;
	}
	//printf("       (file read char %c %d/%d)", perform_bevfs_op_return, ptr, pbuffer);  
		
      case Kiwi_fs_close:
	{
	  int cmd_fd = (perform_bevfs_op_a2>>56) & 0xF;
	  if (verbose > 1) printf("Kiwi fserver: cmd close %d a2=%016llX cmd_fd=%d", perform_bevfs_op_cmd, perform_bevfs_op_a2, cmd_fd);
	  fclose(file_handles[cmd_fd]);
	  uint64_t op_return = (file_handles[cmd_fd] != 0) ? 0: 2;
	  file_handles[cmd_fd] = 0;
	  return op_return;
	}

      default:
	{
	  printf("Kiwi fserver: unsupported Kiwi file system cmd %d a2=%016llX\n", perform_bevfs_op_cmd, perform_bevfs_op_a2);
	  exit(-1);
	}
      }
    }
}


// eof
