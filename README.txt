
Get this code from https://djg11@bitbucket.org/djg11/kiwi-ksubs3.git

# Kiwi Scientific Acceleration: Substrate Code
#
# (C) 2016 DJ Greaves, University of Cambridge, Computer Laboratory.
#
# Kiwi ksubs-server for ksubs3.1 protocol
#
# http://www.cl.cam.ac.uk/research/srg/han/hprls/orangepath/kiwic-demos/zynq-pio-dma/


The ksubs3 features include:

    8 hardware LEDs and switches for very-low level I/O. These may not all be present on certain blades, such as the Parallella.

    Programmed I/O register file mapped into ARM address space.

    File system access is via the Kiwi network-on-chip Noc16, which can be chained through any number of components.

    Abend syndrome reporting (shows running status or reason for stopping).

    Design serial number (simple confidence check that correct design is loaded in the FPGA).

    Hardware waypoint (a method of reporting how far through an application execution has reached).

    Main-thread PC monitor.

    64-bit simple result register (this is the easiest form of output to use to avoid a simple application from being stripped entirely during trimming under FPGA-vendor logic synthesis)

    Run/stop control via clock-enable input (missing currently, Dec 2017). 



The ksubs3.1 is fairly low performance (10 to 20 MB/sec) but 3.2 has per-VC credit-based flow control and has (will have) much greater throughput and an interrupt facility. 

END

